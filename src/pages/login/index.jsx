import './style.css';

import * as yup from 'yup';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import { Redirect, useHistory } from 'react-router-dom';

import axios from 'axios';

const Login = ({ authenticated, setAuthenticated }) => {

    const formSchema = yup.object().shape({
        email: yup.string().required("E-mail obrigatório").email("E-mail inválido"),
        password: yup.string().required("Senha obrigatória").min(8, "Senha deve ter ao menos 8 caracteres"),
    });

    const {
        register,
        handleSubmit,
        formState: { errors }
    } = useForm({
        resolver: yupResolver(formSchema)
    });

    const history = useHistory();

    const onSubmit = (data) => {
        axios.post('https://kenziehub.me/sessions', data)
             .then((response) => {
                const { token, user } = response.data;

                localStorage.setItem('@kenziehub:token', JSON.stringify(token));
                localStorage.setItem('@kenziehub:user', JSON.stringify(user));
                setAuthenticated(true);

                return history.push('/dashboard');
             })
             .catch((err) => console.log(err));
    }

    if (authenticated) {
        return <Redirect to="/dashboard"/>;
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <input
                type='text'
                placeholder='Email'
                {...register('email')}/>
            <p className="yup_error">{errors.email?.message}</p>
            <input
                type='password'
                placeholder='Digite sua senha'
                {...register('password')}/>
            <p className="yup_error">{errors.password?.message}</p>
            <button type='submit'>Entrar</button>
            <p>Não tem uma conta? <a href='/cadastro'><strong>Cadastre-se</strong></a></p>
        </form>
    );
}

export default Login;