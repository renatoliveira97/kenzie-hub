import './style.css';

import * as yup from 'yup';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import { useHistory, Redirect } from 'react-router-dom';

import axios from 'axios';

const Cadastro = ({ authenticated }) => {

    const formSchema = yup.object().shape({
        name: yup.string().required("Nome obrigatório").matches(/[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ]+$/, "O nome deve conter apenas letras"),
        email: yup.string().required("E-mail obrigatório").email("E-mail inválido"),
        password: yup.string().required("Senha obrigatória").min(8, "Senha deve ter ao menos 8 caracteres"),
        bio: yup.string().required("Campo obrigatório"),
        contact: yup.string().required("Link obrigatório"),
        course_module: yup.string().required("Por favor informe o módulo do curso"),
    });

    const {
        register,
        handleSubmit,
        formState: { errors }
    } = useForm({
        resolver: yupResolver(formSchema)
    });
    
    const history = useHistory();

    const onSubmit = (data) => {
        axios.post('https://kenziehub.me/users', data)
             .then((response) => history.push("/login"))
             .catch((err) => console.log(err));
    }

    if (authenticated) {
        return <Redirect to="/dashboard"/>;
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <input
                type='text'
                placeholder='Nome'
                {...register('name')}/>
            <p className="yup_error">{errors.name?.message}</p>
            <input
                type='text'
                placeholder='Email'
                {...register('email')}/>
            <p className="yup_error">{errors.email?.message}</p>
            <input
                type='password'
                placeholder='Digite sua senha'
                {...register('password')}/>
            <p className="yup_error">{errors.password?.message}</p>
            <input
                type='text'
                placeholder='Diga um pouco sobre você'
                {...register('bio')}/>
            <p className="yup_error">{errors.bio?.message}</p>
            <input
                type='text'
                placeholder='Link do seu linkedin'
                {...register('contact')}/>
            <p className="yup_error">{errors.contact?.message}</p>
            <input
                type='text'
                placeholder='Módulo do curso'
                {...register('course_module')}/>
            <p className="yup_error">{errors.course_module?.message}</p>
            <button type='submit'>Cadastrar-se</button>
            <p>Já tem uma conta? Faça o <a href='/login'><strong>login</strong></a></p>               
        </form>
    );
}

export default Cadastro;