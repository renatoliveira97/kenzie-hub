import './style.css';

import { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';

import * as yup from 'yup';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';

import axios from 'axios';

const Dashboard = ({ authenticated }) => {

    const [techs, setTechs] = useState([]);

    const [token] = useState(JSON.parse(localStorage.getItem('@kenziehub:token')) || "");
    const [user] = useState(JSON.parse(localStorage.getItem('@kenziehub:user')) || "");

    const loadTechs = () => {
        axios.get(`https://kenziehub.me/users/{${user.id}}`)
             .then((response) => setTechs(response.data.techs))
             .catch((err) => console.log(err));
    }

    useEffect(() => {
        const firstLoading = () => {
            axios.get(`https://kenziehub.me/users/{${user.id}}`)
                 .then((response) => setTechs(response.data.techs))
                 .catch((err) => console.log(err));
        }
        firstLoading();
    }, [user]);

    const formSchema = yup.object().shape({
        title: yup.string().required("Campo obrigatório"),
        status: yup.string().required("Campo obrigatório"),
    });

    const {
        register,
        handleSubmit,
        formState: { errors }
    } = useForm({
        resolver: yupResolver(formSchema)
    });

    const onSubmit = (data) => {
        axios.post('https://kenziehub.me/users/techs', data, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
             .then((response) => {
                loadTechs();
             })
             .catch((err) => console.log(err));
    }

    if (!authenticated) {
        return <Redirect to="/login"/>;
    }

    const handleDelete = (id) => {
        axios.delete(`https://kenziehub.me/users/techs/{${id}}`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
             .then((response) => {
                loadTechs();
             })
             .catch((err) => console.log(err));
    }

    return (
        <div className='dashboard-container'>
            <div className='create-tech-container'>
                <h3>Adicionar tecnologia</h3>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <input
                        type='text'
                        placeholder='Título'
                        {...register('title')}/>                        
                    <p className="yup_error">{errors.title?.message}</p>
                    <label>
                        <p>Escolha o seu nível de habilidade</p>
                        <select {...register('status')}>
                            <option value='Iniciante'>Iniciante</option>
                            <option value='Intermediário'>Intermediário</option>
                            <option value='Avançado'>Avançado</option>
                        </select>
                    </label>
                    <p className="yup_error">{errors.status?.message}</p>
                    <button type='submit'>Adicionar</button>
                </form>
            </div>
            <div className='view-techs-container'>
                <div>
                    {techs.map((tech) => (
                        <div key={tech.id} className='tech-card'>
                            <h3 className='black'>{tech.title}</h3>
                            <p className='black'><strong>Nível:</strong> {tech.status}</p>
                            <button onClick={() => handleDelete(tech.id)}>Excluir</button>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
}

export default Dashboard;