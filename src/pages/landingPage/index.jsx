import './style.css';

import { useHistory, Redirect } from 'react-router-dom';

const LandingPage = ({ authenticated }) => {

    const history = useHistory();

    if (authenticated) {
        return <Redirect to="/dashboard"/>;
    }

    return (
        <div className="lp-container">
            <button onClick={() => history.push('/login')}>Entrar</button>
            <button onClick={() => history.push('/cadastro')}>Cadastrar-se</button>
        </div>
    );
}

export default LandingPage;