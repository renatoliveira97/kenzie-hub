import { useState, useEffect } from 'react';
import { Switch, Route } from 'react-router-dom';

import LandingPage from '../pages/landingPage';
import Cadastro from '../pages/cadastro';
import Login from '../pages/login';
import Dashboard from '../pages/dashboard';

const Routes = () => {
    const [authenticated, setAuthenticated] = useState(false);

    useEffect(() => {
        const token = JSON.parse(localStorage.getItem('@kenziehub:token'));

        if (token) {
            return setAuthenticated(true);
        }
    }, [authenticated]);

    return (
        <Switch>
            <Route exact path="/">
                <LandingPage authenticated={authenticated}/>
            </Route>
            <Route path="/cadastro">
                <Cadastro authenticated={authenticated}/>
            </Route>
            <Route path="/login">
                <Login authenticated={authenticated} setAuthenticated={setAuthenticated}/>
            </Route>
            <Route path="/dashboard">
                <Dashboard authenticated={authenticated}/>
            </Route>
        </Switch>
    );
}

export default Routes;